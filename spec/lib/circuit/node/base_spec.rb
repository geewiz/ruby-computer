# frozen_string_literal: true

require_relative File.join("..", "..", "..", "spec_helper")

describe Circuit::Node::Base do
  it "updates connected nodes" do
    node1 = described_class.new(component: nil, name: "node1")
    node2 = described_class.new(component: nil, name: "node2")
    node1.connect(node2)

    node1.set(true)

    expect(node2.state).to be true
  end
end
