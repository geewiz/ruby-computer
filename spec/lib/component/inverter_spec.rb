# frozen_string_literal: true

require_relative File.join("..", "..", "spec_helper")

describe Component::Inverter do
  it "evaluates 0 to 1" do
    gate = described_class.new(name: "Inverter")
    gate.set_node(:a, false)

    gate.refresh

    expect(gate.node_state(:b)).to be true
  end

  it "evaluates 1 to 0" do
    gate = described_class.new(name: "Inverter")
    gate.set_node(:a, true)

    gate.refresh

    expect(gate.node_state(:b)).to be false
  end
end
