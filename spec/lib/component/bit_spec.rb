# frozen_string_literal: true

require_relative File.join("..", "..", "spec_helper")

describe Component::Bit do
  it "can be set to 1" do
    circuit = described_class.new(name: "Bit")

    value = true
    circuit.set_node(:i, value)
    circuit.set_node(:s, true)

    circuit.refresh

    expect(circuit.node_state(:q)).to be value
  end

  it "can be set to 0" do
    circuit = described_class.new(name: "Bit")

    value = false
    circuit.set_node(:i, value)
    circuit.set_node(:s, true)

    circuit.refresh

    expect(circuit.node_state(:q)).to be value
  end

  it "remembers being set to 0" do
    circuit = described_class.new(name: "Bit")

    value = false
    circuit.set_node(:i, value)
    circuit.set_node(:s, true)
    circuit.refresh
    circuit.set_node(:s, false)
    circuit.set_node(:i, !value)
    circuit.refresh

    expect(circuit.node_state(:q)).to be value
  end

  it "remembers being set to 1" do
    circuit = described_class.new(name: "Bit")

    value = true
    circuit.set_node(:i, value)
    circuit.set_node(:s, true)
    circuit.refresh
    circuit.set_node(:s, false)
    circuit.set_node(:i, !value)
    circuit.refresh

    expect(circuit.node_state(:q)).to be value
  end
end
