# frozen_string_literal: true

require_relative File.join("..", "..", "spec_helper")

describe Component::NandGate do
  it "evaluates 0 NAND 0 = 1" do
    nand = described_class.new(name: "NAND")
    nand.set_node(:a, false)
    nand.set_node(:b, false)

    nand.refresh

    expect(nand.node_state(:c)).to be true
  end

  it "evaluates 1 NAND 0 = 1" do
    nand = described_class.new(name: "NAND")
    nand.set_node(:a, true)
    nand.set_node(:b, false)

    nand.refresh

    expect(nand.node_state(:c)).to be true
  end

  it "evaluates 0 NAND 1 = 1" do
    nand = described_class.new(name: "NAND")
    nand.set_node(:a, false)
    nand.set_node(:b, true)

    nand.refresh

    expect(nand.node_state(:c)).to be true
  end

  it "evaluates 1 NAND 1 = 0" do
    nand = described_class.new(name: "NAND")
    nand.set_node(:a, true)
    nand.set_node(:b, true)

    nand.refresh

    expect(nand.node_state(:c)).to be false
  end
end
