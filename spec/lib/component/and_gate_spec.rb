# frozen_string_literal: true

require_relative File.join("..", "..", "spec_helper")

describe Component::AndGate do
  it "evaluates 0 AND 0 = 0" do
    component = described_class.new(name: "AND")
    component.set_node(:a, false)
    component.set_node(:b, false)

    component.refresh

    expect(component.node_state(:c)).to be false
  end

  it "evaluates 1 AND 0 = 0" do
    component = described_class.new(name: "AND")
    component.set_node(:a, true)
    component.set_node(:b, false)

    component.refresh

    expect(component.node_state(:c)).to be false
  end

  it "evaluates 0 AND 1 = 0" do
    component = described_class.new(name: "AND")
    component.set_node(:a, false)
    component.set_node(:b, true)

    component.refresh

    expect(component.node_state(:c)).to be false
  end

  it "evaluates 1 AND 1 = 1" do
    component = described_class.new(name: "AND")
    component.set_node(:a, true)
    component.set_node(:b, true)

    component.refresh

    expect(component.node_state(:c)).to be true
  end
end
