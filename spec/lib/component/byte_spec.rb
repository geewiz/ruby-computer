# frozen_string_literal: true

require_relative File.join("..", "..", "spec_helper")

describe Component::Byte do
  it "can be set to a binary value" do
    circuit = described_class.new(name: "Byte")

    circuit.set_node(:i0, true)
    circuit.set_node(:i1, true)
    circuit.set_node(:i2, true)
    circuit.set_node(:i3, true)
    circuit.set_node(:i4, true)
    circuit.set_node(:i5, true)
    circuit.set_node(:i6, true)
    circuit.set_node(:i7, true)
    circuit.set_node(:s, true)

    circuit.refresh

    expect(circuit.node_state(:o0)).to be true
    expect(circuit.node_state(:o1)).to be true
    expect(circuit.node_state(:o2)).to be true
    expect(circuit.node_state(:o3)).to be true
    expect(circuit.node_state(:o4)).to be true
    expect(circuit.node_state(:o5)).to be true
    expect(circuit.node_state(:o6)).to be true
    expect(circuit.node_state(:o7)).to be true
  end

  it "can be set to a decimal value" do
    circuit = described_class.new(name: "Byte")

    circuit.set_input_decimal(42)
    circuit.set_node(:s, true)

    circuit.refresh

    expect(circuit.node_state(:o0)).to be false
    expect(circuit.node_state(:o1)).to be true
    expect(circuit.node_state(:o2)).to be false
    expect(circuit.node_state(:o3)).to be true
    expect(circuit.node_state(:o4)).to be false
    expect(circuit.node_state(:o5)).to be true
    expect(circuit.node_state(:o6)).to be false
    expect(circuit.node_state(:o7)).to be false
  end

  it "stores a decimal value" do
    circuit = described_class.new(name: "Byte")

    circuit.set_input_decimal(42)
    circuit.set_node(:s, true)
    circuit.refresh
    circuit.set_node(:s, false)
    circuit.set_input_decimal(0)
    circuit.refresh

    expect(circuit.get_output_decimal).to be 42
  end
end
