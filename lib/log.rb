# frozen_string_literal: true

def log(message)
  puts message if ENV["DEBUG"]
end
