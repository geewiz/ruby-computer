# frozen_string_literal: true

Dir[File.join(__dir__, '**', '*.rb')].each { |file| require file }
