# frozen_string_literal: true

module Circuit
  module Node
    class Base
      attr_reader :name, :state, :component

      def initialize(component:, name:)
        @name = name
        @state = false
        @component = component
        @stable = true
        @connections = []
      end

      def to_s
        state ? "1" : "0"
      end

      def connect(node)
        log "Connecting #{identifier} to #{node.identifier}"
        @connections << node
        node.set(state)
      end

      def identifier
        "#{component.nil? ? '' : component.name}.#{name}"
      end

      def set(new_state)
        normalized_state = new_state ? true : false
        if normalized_state == @state
          # log "  #{identifier} == #{self}"
          stable
        else
          @state = normalized_state
          log "  #{identifier} => #{self}"
          unstable
          update_connections
        end
      end

      def stable?
        @stable
      end

      private

      def stable
        @stable = true
      end

      def unstable
        @stable = false
      end

      def update_connections
        @connections.each do |node|
          node.set(@state)
        end
      end
    end
  end
end
