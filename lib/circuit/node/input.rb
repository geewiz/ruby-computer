# frozen_string_literal: true

require_relative "base"

module Circuit
  module Node
    class Input < Base
      def set(new_state)
        super
        return if stable?

        @component.needs_refresh
      end
    end
  end
end
