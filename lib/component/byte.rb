# frozen_string_literal: true

module Component
  class Byte < Base
    def setup
      add_input_node(:i0)
      add_input_node(:i1)
      add_input_node(:i2)
      add_input_node(:i3)
      add_input_node(:i4)
      add_input_node(:i5)
      add_input_node(:i6)
      add_input_node(:i7)
      add_input_node(:s)
      add_output_node(:o0)
      add_output_node(:o1)
      add_output_node(:o2)
      add_output_node(:o3)
      add_output_node(:o4)
      add_output_node(:o5)
      add_output_node(:o6)
      add_output_node(:o7)

      @bit0 = add_component(Bit.new(name: "Bit0"))
      @bit1 = add_component(Bit.new(name: "Bit1"))
      @bit2 = add_component(Bit.new(name: "Bit2"))
      @bit3 = add_component(Bit.new(name: "Bit3"))
      @bit4 = add_component(Bit.new(name: "Bit4"))
      @bit5 = add_component(Bit.new(name: "Bit5"))
      @bit6 = add_component(Bit.new(name: "Bit6"))
      @bit7 = add_component(Bit.new(name: "Bit7"))

      node(:i0).connect(@bit0.node(:i))
      node(:s).connect(@bit0.node(:s))
      @bit0.node(:q).connect(node(:o0))
      node(:i1).connect(@bit1.node(:i))
      node(:s).connect(@bit1.node(:s))
      @bit1.node(:q).connect(node(:o1))
      node(:i2).connect(@bit2.node(:i))
      node(:s).connect(@bit2.node(:s))
      @bit2.node(:q).connect(node(:o2))
      node(:i3).connect(@bit3.node(:i))
      node(:s).connect(@bit3.node(:s))
      @bit3.node(:q).connect(node(:o3))
      node(:i4).connect(@bit4.node(:i))
      node(:s).connect(@bit4.node(:s))
      @bit4.node(:q).connect(node(:o4))
      node(:i5).connect(@bit5.node(:i))
      node(:s).connect(@bit5.node(:s))
      @bit5.node(:q).connect(node(:o5))
      node(:i6).connect(@bit6.node(:i))
      node(:s).connect(@bit6.node(:s))
      @bit6.node(:q).connect(node(:o6))
      node(:i7).connect(@bit7.node(:i))
      node(:s).connect(@bit7.node(:s))
      @bit7.node(:q).connect(node(:o7))
    end

    def set_input_decimal(number)
      binary = convert_to_binary(number)
      set_node(:i0, binary.pop)
      set_node(:i1, binary.pop)
      set_node(:i2, binary.pop)
      set_node(:i3, binary.pop)
      set_node(:i4, binary.pop)
      set_node(:i5, binary.pop)
      set_node(:i6, binary.pop)
      set_node(:i7, binary.pop)
    end

    def get_output_decimal
      (node_state_numerical(:o7) * 128) +
        (node_state_numerical(:o6) * 64) +
        (node_state_numerical(:o5) * 32) +
        (node_state_numerical(:o4) * 16) +
        (node_state_numerical(:o3) * 8) +
        (node_state_numerical(:o2) * 4) +
        (node_state_numerical(:o1) * 2) +
        (node_state_numerical(:o0) * 1)
    end

    private

    def convert_to_binary(number)
      binary = []
      divisor = 128
      while divisor >= 1
        if number >= divisor
          binary << true
          number -= divisor
        else
          binary << false
        end
        divisor /= 2
      end

      binary
    end
  end
end
