# frozen_string_literal: true

module Component
  class Inverter < Base
    def setup
      add_input_node(:a)
      add_output_node(:b)

      nand = add_component(Component::NandGate.new(name: "NAND"))
      nand.node(:a).connect(nand.node(:b))

      node(:a).connect(nand.node(:a))
      nand.node(:c).connect(node(:b))
    end
  end
end
