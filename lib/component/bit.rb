# frozen_string_literal: true

module Component
  class Bit < Base
    def setup
      add_input_node(:i)
      add_input_node(:s)
      add_output_node(:q)

      nand1 = add_component(NandGate.new(name: "NAND1"))
      nand2 = add_component(NandGate.new(name: "NAND2"))
      nand3 = add_component(NandGate.new(name: "NAND3"))
      nand4 = add_component(NandGate.new(name: "NAND4"))

      node(:i).connect(nand1.node(:a))
      node(:s).connect(nand1.node(:b))

      nand1.node(:c).connect(nand2.node(:a))
      node(:s).connect(nand2.node(:b))

      nand1.node(:c).connect(nand3.node(:a))
      nand4.node(:c).connect(nand3.node(:b))

      nand3.node(:c).connect(nand4.node(:a))
      nand2.node(:c).connect(nand4.node(:b))

      nand3.node(:c).connect(node(:q))
    end
  end
end
