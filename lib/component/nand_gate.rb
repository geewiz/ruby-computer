# frozen_string_literal: true

module Component
  class NandGate < Base
    def setup
      add_input_node(:a)
      add_input_node(:b)
      add_output_node(:c)
    end

    def needs_refresh?
      @needs_refresh
    end

    private

    def eval
      set_node(:c, !(node_state(:a) and node_state(:b)))
      @needs_refresh = false
    end
  end
end
