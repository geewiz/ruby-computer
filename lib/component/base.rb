# frozen_string_literal: true

module Component
  class Base
    attr_reader :name

    def initialize(name:)
      @name = name
      @nodes = {}
      @components = []

      setup

      @needs_refresh = true
      refresh
      log "#{name} initialized"
    end

    def setup
      raise "Component class is missing #setup"
    end

    def format_state
      "#{name}: " +
        @nodes.map do |name, node|
          format("%<name>s=%<state>s", name: name, state: node)
        end.join(", ")
    end

    def refresh
      return unless @needs_refresh

      eval
      log format_state
      @needs_refresh = false
    end

    def node(name)
      @nodes[name]
    end

    def set_node(node_name, state)
      node(node_name).set(state)
    end

    def node_state(name)
      node(name).state
    end

    def node_state_numerical(name)
      node(name).state ? 1 : 0
    end

    def add_component(component)
      @components << component
      component
    end

    def needs_refresh?
      @components.empty? || @components.any?(&:needs_refresh?)
    end

    def needs_refresh
      @needs_refresh = true
    end

    private

    def add_input_node(name)
      @nodes[name] = Circuit::Node::Input.new(component: self, name: name)
    end

    def add_output_node(name)
      @nodes[name] = Circuit::Node::Output.new(component: self, name: name)
    end

    def eval
      attempts = 0
      while needs_refresh?
        @components.each(&:refresh)

        attempts += 1
        raise "#{name} did not reach stable state" if attempts > 5
      end
    end
  end
end
