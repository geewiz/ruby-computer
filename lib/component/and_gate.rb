# frozen_string_literal: true

module Component
  class AndGate < Base
    def setup
      add_input_node(:a)
      add_input_node(:b)
      add_output_node(:c)

      nand = add_component(Component::NandGate.new(name: "NAND"))
      inverter = add_component(Component::Inverter.new(name: "Inverter"))
      nand.node(:c).connect(inverter.node(:a))

      node(:a).connect(nand.node(:a))
      node(:b).connect(nand.node(:b))
      inverter.node(:b).connect(node(:c))
    end
  end
end
